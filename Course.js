const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	slots: {
		type: Number,
		required: [true, "Slots is required"]
	},
	createdOn: {
		type: Date,

		// The "new Date ()" expresssion instatiates a new "data" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			usernameL {
				type: String,
				required: [true, "Username is required"]
			}
			isPaid: {
				type: Boolean,
				default: true
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}

	]	

})
module.exports = mongoose.model("Course", courseSchema);


const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "firstName is required"]
	},
	lastName: {
		type: String,
		required: [true, "lastName is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "password Id is required"]
	},
	isAdmin: {
		type: Boolean,required: 
		[false, "Admin is required"]
	},
	mobileNo: {
		type: Number,	
		required: [true, "Mobile number is required"]	
	},
	enrollments [
		{
			courseID: {
				type: Number,
				required: [true, "Course Id is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String
				default: Enrolled()
			}
		}
	]


})
module.exports = mongoose.model("User",userSchema);

